// Check first whether the letter is a single character.
// If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
// If letter is invalid, return undefined.

// Function to count the number of occurrences of a letter in a sentence
function countLetter(letter, sentence) 
{
    // Check if letter is only one character long
    if (letter.length !== 1) 
    {
        return undefined;
    }

    let count = 0;

    // Iterate through each character of the sentence
    for (let i = 0; i < sentence.length; i++) 
    {
        // Check if character matches the letter
        if (sentence[i] === letter)
        {
            // Increment count if it does
            count++;
        }
    }

    // Return the total count
    return count;
}



// An isogram is a word where there are no repeating letters.
// The function should disregard text casing before doing anything else.
// If the function finds a repeating letter, return false. Otherwise, return true.
// Convert the text to lowercase to disregard casing
    
// This function checks if a given string is an isogram or not
function isIsogram(text) 
{
    // Convert the text to lowercase
    text = text.toLowerCase();

     // Create a new set to store the characters
    const seen = new Set();
    
     // Iterate through each character in the text
    for (let i = 0; i < text.length; i++) 
    {
        // Check if the character has already been seen
        if (seen.has(text[i])) 
        {
            // If it has been seen, return false
            return false;
        }

        // Add the character to the set
        seen.add(text[i]);
    }

    // If all characters have been seen, return true
    return true;
}



// Return undefined for people aged below 13.
// Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
// Return the rounded off price for people aged 22 to 64.
// The returned value should be a string.

// Function to calculate discounted price based on age
function purchase(age, price) 
{
    let discountedPrice;

     // Calculate discounted price for ages 13-21
    if (age >= 13 && age <= 21) 
    {
        discountedPrice = price * 0.8;
    } 
    // No discount for ages 21-65
    else if (age > 21 && age < 65) 
    {
        discountedPrice = price;
    }
    // Calculate discounted price for ages 65+
    else if (age >= 65) 
    {
        discountedPrice = price * 0.8;
    }
    // Return undefined if age is not valid
    else 
    {
        discountedPrice = undefined;
    }

     // Return discounted price with two decimal places or undefined
    return discountedPrice ? discountedPrice.toFixed(2) : undefined;
}



// Find categories that has no more stocks.
// The hot categories must be unique; no repeating categories.
// The passed items array from the test are the following:
// { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
// { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
// { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
// { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
// { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
// The expected output after processing the items array is ['toiletries', 'gadgets'].
// Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

// Function to find hot categories from a list of items
function findHotCategories(items) 
{
    // Array to store hot categories
    const hotCategories = [];

     // Set to store unique categories
    const categories = new Set();
    
     // Loop through all items
    for (let i = 0; i < items.length; i++) 
    {
        const item = items[i];

         // Check if item has 0 stocks
        if (item.stocks === 0) 
        {
            // Check if category is not already in set
            if (!categories.has(item.category)) 
            {
                // Add category to hot categories array
                hotCategories.push(item.category);

                 // Add category to set
                categories.add(item.category);
            }
        }
    }

     // Return array of hot categories
    return hotCategories;
}



// Find voters who voted for both candidate A and candidate B.
// The passed values from the test are the following:
// candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
// candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']
// The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
// Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

// This function takes two arrays of voters and returns an array of flying voters
function findFlyingVoters(candidateA, candidateB) 
{
    // Filter through the first array of voters and return only those that are also in the second array
    const flyingVoters = candidateA.filter(voter => candidateB.includes(voter));

    // Return the filtered array of flying voters
    return flyingVoters;
}

 // Declare two arrays of voters
const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

 // Call the function and store the returned array of flying voters
const flyingVoters = findFlyingVoters(candidateA, candidateB);



module.exports = 
{
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};